# Continuous Integration of a C++ project with GitLab

[![pipeline status](https://gitlab.com/gitlabci-demo/cpp-demo/badges/master/pipeline.svg)](https://gitlab.com/gitlabci-demo/cpp-demo/commits/master)

Simple C++ project based on CMake and Catch (for unit test).

## How to build

CMake >= 3.2 and a C++14 compiler are required:

```bash
apt-get install -y build-essential cmake
```

Then generate the build files with cmake before building with make:

```bash
cd cpp-demo
mkdir build && cd build
cmake ..
make
make test
```
